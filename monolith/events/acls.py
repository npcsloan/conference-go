from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    query = {"query": f"{city} {state}", "per_page":"1"}

    response = requests.get(url, headers=headers, params=query)

    if response.status_code == 200:
        data = json.loads(response.content)
        photo_url = data["photos"][0]["src"]["original"]
        return {"photo_url": photo_url}

    else:
        return {"error": "Failed to fetch photo."}

def get_weather(city, state):
    response = requests.get(f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}")

    if response.status_code == 200:
        data = json.loads(response.content)
        if not data:
            return "Invalid location."
        lat, lon = data[0]["lat"], data[0]["lon"]

    else:
        return f"Error: {response.status_code}"

    response = requests.get(f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}")

    if response.status_code == 200:
        data = json.loads(response.content)
        description = data["weather"][0]["description"]
        temp = data["main"]["temp"]
        temp_f = int((temp - 273.15) * 1.8 + 32)

    else:
        return f"Error: {response.status_code}"

    return {
        "description": description,
        "temp": temp_f,
    }
